package com.jean.beans;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class BeanConApplicationScoped implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7346362379254236693L;

	private String  Saludo ;

	public String getSaludo() {
		return Saludo;
	}

	public void setSaludo(String saludo) {
		Saludo = saludo;
	}
	
	
	
}
