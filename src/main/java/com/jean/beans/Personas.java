package com.jean.beans;

import java.io.Serializable;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;




@ManagedBean
@SessionScoped

public class Personas implements Serializable {

	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1742960014814255322L;

	private String nombre;
	
	private String apellido;

	private int edad;
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	
	
	
	public String getCalculoDeEdad(){
		
		if(this.getEdad()>=18){
			
			return "EdadMayor.xhtml";
		}else{
			
			return "EdadMenor.xhtml";
			}
		
	}
	
	
}
